//const express = require("express")

const User = require('../models/index')
const logger = require('../commons/logger');
var express = require('express'),
router = express.Router();

    // Add a binding for '/users/'
router .get('/', function (req, res){
    // render the /users view
    User.findAll().then((users) => {
        if (users) {
            res.json(users)
        } else {
            res.status(404).send("Users not found");
        }
    }).catch((err) => {
        console.error("Error:", err);
        res.status(500).send("Internal Server error %s", err);
    });
  })

  router.get('/users/:userId', function (req, res){
    // render the /users view
    User.findAll({
        where: {
            id: req.params.userId
        }
    }).then((user) => {
        if (user)
            res.json(user)
        else
            res.status(404).send("User not found with id", req.params.userId);

    }).catch((err) => {
        console.error("Error:", err);
        res.status(500).send("Internal Server error %s", err);
    })
  })

  router.put('/users', function (req, res){
    logger.info("trying to add user");
    if (req.body.userid) {
        User.findByPk(req.body.userid).then((user) => {
            if (user) {
                user.update({
                    username: req.body.username,
                    gender: req.body.gender,
                    mobile_num: req.body.gender,
                    is_deleted: false,
                    is_active: true
                }).then((user) => {
                    res.json(user);
                })
            } else {
                res.status(404).send("User not found with id ${req.body.id}" + req.body.id);
            }
        })

    } else {
        logger.info("trying to add user");
        const newUser = User.create({
            userid:req.body.userid,
            username: req.body.username,
            gender: req.body.gender,
            mobile_num: req.body.gender,
            is_deleted: false,
            is_active: true,
        }).then((user) => {
            
            res.json(user);
        }).catch((err) => {
            console.error("Error:", err);
            res.status(500).send("Internal Server error %s", err);
        })
    }
  })

module.exports = router;