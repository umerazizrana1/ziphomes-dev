const Sequelize = require('sequelize')
//Connect DB
const sequelize = require('../db/pg');

const logger = require('../commons/logger');
const db = require('../models/index')
var express = require('express'),
    router = express.Router();
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
//for bcrypt
const bcrypt = require('bcrypt');
const { connectionString } = require('pg/lib/defaults');
const accessTokenSecret = process.env.JWT_TOKEN_SECRET;
const saltRounds = parseInt(process.env.JWT_SALT_ROUNDS);


router.post('/', function (req, res) {
    // Read username and password from request body
    const { email, password } = req.body;

    //hash passowrd using bcrypt
    let calculatedHash = bcrypt.hashSync(req.body.password, saltRounds);

    logger.info('Login Request Recieved for : ' + email);

    db.User.findOne({
        where: {
            email: req.body.email
        }, include: ["roles"]
    }).then((user) => {
        if (user) {
            const match = bcrypt.compareSync(req.body.password, user.password);
            if (match) {
                logger.info('Login success..' + user.email)
                // Generate an access token
                const accessToken = jwt.sign({ username: user.username, role: user.role }, accessTokenSecret,
                    { expiresIn: process.env.JWT_EXPIRES_IN });

                var data = {};
                const username = user.username;
                const email = user.email;
                const userid = user.user_id;
                const role = '';
                const role_id = '';
                const userrole = '';
                for (let user_role of user.toJSON().roles) {
                    logger.info('user role: '+user_role);

                    const role = user_role.role_name;
                    const role_id = user_role.role_id;

                    res.json({
                        accessToken, username, email, userid, role, user, userrole
                    });



                }


            } else {
                logger.info('password validation failed.. ' + email);
                res.status(404).send("Username or password incorrect");
            }

        }
        else {
            logger.info('login failed.. ' + email);
            res.status(404).send("Username or password incorrect");
        }
    }).catch((err) => {
        logger.error('Error:', err);
        res.status(500).send("Internal Server error" + err);
    })

})

router.get('/authenticateJWT', function (req, res) {
    const authHeader = req.headers.authorization;
    console.log("authHeader %s", authHeader);
    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err)
                console.error(err);
            if (err) {
                return res.sendStatus(403);
            }
            console.log("user : %s", user);
            req.user = user;
            next();
        });
    } else {
        res.sendStatus(401);
    }
})

module.exports = router;