const AWS = require('aws-sdk');
const logger = require('./logger');

const fs = {};

// Replace actual strings with configs
const ID = process.env.AWS_ACCESS_KEY;
const SECRET = process.env.AWS_SECRET_KEY;
const bucketName = process.env.AWS_BUCKET;

const s3 = new AWS.S3({
    accessKeyId: ID,
    secretAccessKey: SECRET
})

fs.upload = (propertyId, fileName, data) => {
    var key = propertyId + '/' + fileName
    logger.info('uploading file with key: ${key}');

    //set up all the required params
    var objectParams = {
        Bucket: bucketName, 
        Key: key, 
        Body: data
    };
    //upload to s3 using promise


    var uploadPromise = s3.putObject(objectParams).promise();
    uploadPromise.then(
        function (data) {
            logger.info('Successfully uploaded data to ' + bucketName + '/' + key);
            return true;
        }).catch((err)=>{
            logger.error('Error: ', err);
            return false;
        });

}

fs.download = (propertyId, fileName) =>{
    var key = propertyId + '/' + fileName

    logger.info('Downloading file with key: ${key}')
    //setting params for download
    var objectParams = {
        Bucket: bucketName,
        Key: key
    }
    //calling s3 getObject for content stream
    var stream = s3.getObject(objectParams).createReadStream();
    //verify if object is present
    if(stream){
        logger.info('object found...');
        return stream;
    }else{
        logger.error("object not found..");
        return null;
    }

}

module.exports = fs;