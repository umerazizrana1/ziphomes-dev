require('dotenv').config();
const Sequelize = require('sequelize');
const logger = require('../commons/logger');

//getting params from env
const user = process.env.POSTGRES_USER
const host = process.env.POSTGRES_HOST
const database = process.env.POSTGRES_DB
const password = process.env.POSTGRES_PWD
const port = process.env.POSTGRES_PORT
//const sequelize = new Sequelize(postgresUrl)

const sequelize = new Sequelize(database, user, password, {
    host,
    port,
    dialect: 'postgres',
    logging: true,
    // pool configuration used to pool database connections
    pool: {
        min: 5,
        max: 10,
        idle: 30000,
        acquire: 60000,
    },
})

sequelize.authenticate().then(() => {

    logger.debug('Connection has been established successfully.');

}).catch(err => {

    logger.error('Unable to connect to the database:', err);

});

module.exports = sequelize;