const express = require('express');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const logger = require('./commons/logger');

const app = express()

const port = 5000

app.use(express.json());

//require('./routes/routes');

var appRoutes = require('./routes/routes');

// Import my test routes into the path '/test'
app.use('/', appRoutes);

// require("./routes/routes")(app);

logger.info("server is starting");


//const userApi = require('./user/users');
// const groupApi = require('./group/groups');
// const loginApi = require('./login/login');

// app.get('/users/:userId', userApi.getUserById);
// app.get('/users', loginApi.authenticateJWT, userApi.getAllUsers);
// app.put('/users', userApi.addUpdateUser);
// app.get('/groups/:groupId', groupApi.getGroupById);
// app.get('/groups', groupApi.getAllGroups);
// app.post('/login', loginApi.login);



app.use(bodyParser.json());

app.get('/', (req, res) => res.json({ message: 'Welcome to Zip Homes' }))

app.listen(port, () => console.log(`App listening on port ${port}!`))




app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));