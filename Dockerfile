FROM node:12

ENV POSTGRES_URL=postgres://postgres:oracle@192.168.100.229:5432/zhomes
ENV POSTGRES_USER=postgres
ENV POSTGRES_HOST=ziphomes-db-cluster-instance-1.clqt9jd0aqdn.us-east-1.rds.amazonaws.com
ENV POSTGRES_DB=ziphomesdb
ENV POSTGRES_PWD=Y9ZkaWtQsobwVEEppMEF
ENV POSTGRES_PORT=5432
ENV NODE_ENV=DEV

# Create app directory and epgpub directory
RUN mkdir /src
WORKDIR /src

# Install app dependencies
ADD package.json /src/package.json
RUN npm install

#Bundle app source
COPY . /src

EXPOSE 5000

CMD npm start
