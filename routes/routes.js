var express = require('express'), router = express.Router();

var users = require('../user/users');
// var groups = require('../group/groups');
var login = require('../login/login');

router.use('/users', users);
// router.use('/groups', groups);
router.use('/login',login);
 
module.exports = router;