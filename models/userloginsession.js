
const Sequelize = require('sequelize');
//Connect DB
const sequelize = require('../db/pg');


module.exports = (sequelize, DataTypes) => {
    const UserLoginSession = sequelize.define('tbl_users_login_session', {

        // attributes
        log_id: {
            type:  Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        session_id: {
            type: Sequelize.STRING,
            allowNull: false
        },
        expire_timestamp: {
            type: "TIMESTAMP",  
            defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
            allowNull: false,
        },
        created_at: {
            type: "TIMESTAMP",
            defaultValue: Sequelize.literal("CURRENT_TIMESTAMP")
        },
        created_by: {
            type: Sequelize.STRING,
        },
        updated_at: {
            type: "TIMESTAMP",  // To enforce a data type
            defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
            allowNull: false,
        },
        updated_by: {
            type: Sequelize.STRING,
        }
    
    }, {
    
        // options
        // To disable default timestamps columns
        timestamps: false,
        //To enforce table name and stop creating table name as users instead of user
        freezeTableName: true
    
    
    });
    return UserLoginSession;
  };


// Note: using `force: true` will drop the table if it already exists
// UserLoginSession.sync({ alter: true }) // Now the `tbl_users_login_session` table in the database corresponds to the model definition
// module.exports = UserLoginSession;

