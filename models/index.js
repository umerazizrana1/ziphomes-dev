const Sequelize = require('sequelize')
//Connect DB
const sequelize = require('../db/pg');

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.User = require("./user")(sequelize, Sequelize);
db.UserLoginSession = require("./userloginsession")(sequelize, Sequelize);
db.Roles = require("./role")(sequelize, Sequelize);
db.Permission = require("./permission")(sequelize, Sequelize);


db.User.hasMany(db.UserLoginSession , 
  {  
    foreignKey: 'user_id',
    as: "userloginsessions" }
  );

db.UserLoginSession.belongsTo(db.User ,{
  foreignKey: 'user_id',
  as: "user",
}
);

db.User.belongsToMany(db.Roles, {
  through: "tbl_user_roles",
  as: "roles",
  foreignKey: "user_id",
       // To disable default timestamps columns
  timestamps: false
});
db.Roles.belongsToMany(db.User, {
  through: "tbl_user_roles",
  as: "users",
  foreignKey: "role_id",
  // To disable default timestamps columns
  timestamps: false
});


db.Roles.belongsToMany(db.Permission, {
  through: "tbl_role_permissions",
  as: "permissions",
  foreignKey: "role_id",
       // To disable default timestamps columns
  timestamps: false
});

db.Permission.belongsToMany(db.Roles, {
  through: "tbl_role_permissions",
  as: "roles",
  foreignKey: "permission_id",
  // To disable default timestamps columns
  timestamps: false
});


sequelize.sync({alter: true} );
module.exports = db;