
const Sequelize = require('sequelize')
//Connect DB
const sequelize = require('../db/pg');
// const UserRoles = require('../user/userroles');
// const RolePermissions = require('./rolepermissions');



module.exports = (sequelize, DataTypes) => {
const Roles = sequelize.define('tbl_roles', {

    // attributes

    role_id: {
        type:  Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    role_name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    role_description: {
        type: Sequelize.TEXT,
        allowNull: true
    },
    is_active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    },   
    is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    created_at: {
        type: "TIMESTAMP",
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP")
    },
    created_by: {
        type: Sequelize.STRING,
        allowNull: false
    },
    updated_at: {
        type: "TIMESTAMP",  // To enforce a data type
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
        allowNull: false,
    },
    updated_by: {
        type: Sequelize.STRING,
        allowNull: false
    }

}, {

    // options
    // To disable default timestamps columns
    timestamps: false,
    //To enforce table name and stop creating table name as users instead of user
    freezeTableName: true


});
 return Roles;
};

// Note: using `force: true` will drop the table if it already exists

// Roles.sync({ alter: true }) // Now the `tbl_user` table in the database corresponds to the model definition
// // Roles.hasMany(UserRoles);
// module.exports = Roles;


