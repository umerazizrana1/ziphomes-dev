
const Sequelize = require('sequelize')
//Connect DB
const sequelize = require('../db/pg');
// const RolePermissions = require('./rolepermissions');

module.exports = (sequelize, DataTypes) => {
const Permission = sequelize.define('tbl_permissions', {

    // attributes

    permission_id: {
        type:  Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    permission_name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    parent_permission_id: {
        type: Sequelize.STRING,
        allowNull: true
    },
    order: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    is_active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    },   
    action:{
        type: Sequelize.STRING,
        allowNull: false
    },
    created_at: {
        type: "TIMESTAMP",
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP")
    },
    created_by: {
        type: Sequelize.STRING,
    },
    updated_at: {
        type: "TIMESTAMP",  // To enforce a data type
        defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
        allowNull: false,
    },
    updated_by: {
        type: Sequelize.STRING,
    }

}, {

    // options
    // To disable default timestamps columns
    timestamps: false,
    //To enforce table name and stop creating table name as users instead of user
    freezeTableName: true


});

  return Permission;
};

// // Note: using `force: true` will drop the table if it already exists

// Permission.sync({ alter: true }) // Now the `tbl_user` table in the database corresponds to the model definition

// const RolePermissions = sequelize.define('tbl_role_permissions', {

//     // attributes

//     role_id: {
//         type:  Sequelize.STRING,
//         allowNull: false,
//     },
//     permission_id: {
//         type: Sequelize.STRING,
//         allowNull: false
//     }

// }, {

//     // options
//     // To disable default timestamps columns
//     timestamps: false,
//     //To enforce table name and stop creating table name as users instead of user
//     freezeTableName: true


// });

// // Note: using `force: true` will drop the table if it already exists

// RolePermissions.sync({ alter: true }) // Now the `tbl_user` table in the database corresponds to the model definition

// module.exports = RolePermissions;

// module.exports = Permission;


