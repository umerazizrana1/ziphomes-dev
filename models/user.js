
const Sequelize = require('sequelize')
//Connect DB
const sequelize = require('../db/pg');
const saltRounds = parseInt(process.env.saltRounds);
module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('tbl_user', {

        // attributes
    
        user_id: {
            type:  Sequelize.STRING,
            allowNull: false,
            primaryKey: true,
        },
        username: {
            type: Sequelize.STRING,
            allowNull: false
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false
            // allowNull defaults to true
    
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
            // allowNull defaults to true
        },   
        gender:{
            type: Sequelize.STRING,
            allowNull: false
        },
        mobile_num:{
            type: Sequelize.STRING,
            allowNull: false
        },
        created_at: {
            type: "TIMESTAMP",
            defaultValue: Sequelize.literal("CURRENT_TIMESTAMP")
        },
        created_by: {
            type: Sequelize.STRING,
        },
        updated_at: {
            type: "TIMESTAMP",  // To enforce a data type
            defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
            allowNull: false,
        },
        updated_by: {
            type: Sequelize.STRING,
        },
        is_deleted:{
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        is_active: {
            type: Sequelize.BOOLEAN,
            defaultValue: true
        }
    
    }, {
    
        // options
        // To disable default timestamps columns
        timestamps: false,
        //To enforce table name and stop creating table name as users instead of user
        freezeTableName: true,
        instanceMethods: {
            generateHash(password) {
                return bcrypt.hash(password, saltRounds);
            },
            validPassword(password) {
                return bcrypt.compare(password, this.password);
            }
        }
     
    
    });
    return User;
  };



// // Note: using `force: true` will drop the table if it already exists

// User.sync({  alter: true  }) // Now the `tbl_user` table in the database corresponds to the model definition

// const UserLoginSession = sequelize.define('tbl_users_login_session', {

//     // attributes
//     log_id: {
//         type: Sequelize.STRING,
//         allowNull: false,
//         primaryKey: true,
//     },
//     user_id: {
//         type: Sequelize.STRING,
//         allowNull: false,
//     },
//     session_id: {
//         type: Sequelize.STRING,
//         allowNull: false
//     },
//     expire_timestamp: {
//         type: "TIMESTAMP",  
//         defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
//         allowNull: false,
//     },
//     created_at: {
//         type: "TIMESTAMP",
//         defaultValue: Sequelize.literal("CURRENT_TIMESTAMP")
//     },
//     created_by: {
//         type: Sequelize.STRING,
//     },
//     updated_at: {
//         type: "TIMESTAMP",  // To enforce a data type
//         defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
//         allowNull: false,
//     },
//     updated_by: {
//         type: Sequelize.STRING,
//     }

// }, {

//     // options
//     // To disable default timestamps columns
//     timestamps: false,
//     //To enforce table name and stop creating table name as users instead of user
//     freezeTableName: true


// });
// UserLoginSession.sync({  alter: true  })


// const UserRoles = sequelize.define('tbl_user_roles', {

//     // attributes
//     user_id: {
//         type: Sequelize.STRING,
//         allowNull: false
//     },
//     role_id: {
//         type: Sequelize.STRING,
//         allowNull: false
//     }
// }, {

//     // options
//     // To disable default timestamps columns
//     timestamps: false,
//     //To enforce table name and stop creating table name as users instead of user
//     freezeTableName: true

// });

// // Note: using `force: true` will drop the table if it already exists

// UserRoles.sync({  alter: true }) // Now the `tbl_users_login_session` table in the database corresponds to the model definition


// const Roles = sequelize.define('tbl_roles', {

//     // attributes

//     role_id: {
//         type:  Sequelize.STRING,
//         allowNull: false,
//         primaryKey: true,
//     },
//     role_name: {
//         type: Sequelize.STRING,
//         allowNull: false
//     },
//     role_description: {
//         type: Sequelize.TEXT,
//         allowNull: true
//     },
//     is_active: {
//         type: Sequelize.BOOLEAN,
//         defaultValue: true
//     },   
//     is_deleted: {
//         type: Sequelize.BOOLEAN,
//         defaultValue: false
//     },
//     created_at: {
//         type: "TIMESTAMP",
//         defaultValue: Sequelize.literal("CURRENT_TIMESTAMP")
//     },
//     created_by: {
//         type: Sequelize.STRING,
//         allowNull: false
//     },
//     updated_at: {
//         type: "TIMESTAMP",  // To enforce a data type
//         defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
//         allowNull: false,
//     },
//     updated_by: {
//         type: Sequelize.STRING,
//         allowNull: false
//     }

// }, {

//     // options
//     // To disable default timestamps columns
//     timestamps: false,
//     //To enforce table name and stop creating table name as users instead of user
//     freezeTableName: true


// });

// // Note: using `force: true` will drop the table if it already exists

// Roles.sync({ alter: true }) // Now the `tbl_user` table in the database corresponds to the model definition

// const Permission = sequelize.define('tbl_permissions', {

//     // attributes

//     permission_id: {
//         type:  Sequelize.STRING,
//         allowNull: false,
//         primaryKey: true,
//     },
//     permission_name: {
//         type: Sequelize.STRING,
//         allowNull: false
//     },
//     parent_permission_id: {
//         type: Sequelize.STRING,
//         allowNull: true
//     },
//     is_active: {
//         type: Sequelize.BOOLEAN,
//         defaultValue: true
//     },   
//     action:{
//         type: Sequelize.STRING,
//         allowNull: false
//     },
//     mobile_num:{
//         type: Sequelize.STRING,
//         allowNull: false
//     },
//     created_at: {
//         type: "TIMESTAMP",
//         defaultValue: Sequelize.literal("CURRENT_TIMESTAMP")
//     },
//     created_by: {
//         type: Sequelize.STRING,
//     },
//     updated_at: {
//         type: "TIMESTAMP",  // To enforce a data type
//         defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
//         allowNull: false,
//     },
//     updated_by: {
//         type: Sequelize.STRING,
//     }

// }, {

//     // options
//     // To disable default timestamps columns
//     timestamps: false,
//     //To enforce table name and stop creating table name as users instead of user
//     freezeTableName: true


// });

// // Note: using `force: true` will drop the table if it already exists

// Permission.sync({ alter: true }) // Now the `tbl_user` table in the database corresponds to the model definition

// const RolePermissions = sequelize.define('tbl_role_permissions', {

//     // attributes

//     role_id: {
//         type:  Sequelize.STRING,
//         allowNull: false,
//     },
//     permission_id: {
//         type: Sequelize.STRING,
//         allowNull: false
//     }

// }, {

//     // options
//     // To disable default timestamps columns
//     timestamps: false,
//     //To enforce table name and stop creating table name as users instead of user
//     freezeTableName: true


// });

// // Note: using `force: true` will drop the table if it already exists

// RolePermissions.sync({ alter: true }) // Now the `tbl_user` table in the database corresponds to the model definition




// User.hasMany(UserLoginSession , {
//     foreignKey: 'user_id'
//   });

// UserLoginSession.belongsTo(User);
// User.hasMany(UserRoles, {
//     foreignKey: 'user_id'
//   })

// Roles.hasMany(UserRoles,{
//     foreignKey: 'role_id'
// });

// UserRoles.belongsTo(User);
// UserRoles.belongsTo(Roles);

// Roles.hasMany(RolePermissions,{
//     foreignKey: 'role_id'
// });
// Permission.hasMany(RolePermissions , {
//     foreignKey: 'permission_id'
// });

// RolePermissions.belongsTo(Permission);
// RolePermissions.belongsTo(Roles);


// module.exports = User;
// module.exports = UserLoginSession;
// module.exports = Roles;
// module.exports = UserRoles;
// module.exports = RolePermissions;
// module.exports = Permission;

