
const Sequelize = require('sequelize')
//Connect DB
const sequelize = require('../db/pg');
// const Permission = require('./permission');

module.exports = (sequelize, DataTypes) => {
const RolePermissions = sequelize.define('tbl_role_permissions', {

    // attributes

    role_id: {
        type:  Sequelize.STRING,
        allowNull: false,
    },
    permission_id: {
        type: Sequelize.STRING,
        allowNull: false
    }

}, {

    // options
    // To disable default timestamps columns
    timestamps: false,
    //To enforce table name and stop creating table name as users instead of user
    freezeTableName: true


});
  return RolePermissions;
};
// Note: using `force: true` will drop the table if it already exists

// RolePermissions.sync({ alter: true }) // Now the `tbl_user` table in the database corresponds to the model definition

// module.exports = RolePermissions;


