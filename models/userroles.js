
const Sequelize = require('sequelize')
//Connect DB
const sequelize = require('../db/pg');
// const Roles = require('../permissions/role');
// const User = require('./user');

module.exports = (sequelize, DataTypes) => {
const UserRoles = sequelize.define('tbl_user_roles', {

    // attributes
    user_id: {
        type: Sequelize.STRING,
        allowNull: false
    },
    role_id: {
        type: Sequelize.STRING,
        allowNull: false
    }
}, {

    // options
    // To disable default timestamps columns
    timestamps: false,
    //To enforce table name and stop creating table name as users instead of user
    freezeTableName: true

});
  return UserRoles
};

// Note: using `force: true` will drop the table if it already exists

// UserRoles.sync({  alter: true }) // Now the `tbl_users_login_session` table in the database corresponds to the model definition
// module.exports = UserRoles;

